import java.util.Scanner;

public class Schedule {
    //просто шаг
    private static final Time STEP = new Time(0, 15);

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        //время завтрака
        System.out.print("Время вашего завтрака: ");
        Time breakfast = new Time(scanner.nextLine());

        //время ужина
        System.out.print("Время вашего ужина: ");
        Time supper = new Time(scanner.nextLine());

        //интервал приёма таблеток
        System.out.print("Какой интервал между приёмами таблеток нужно соблюдать: ");
        Time tabletInterval = new Time(scanner.nextLine());

        //расстояние между приёмом таблеток и приёмом пищи
        System.out.print("За сколько до приёма еды нужно принять таблетку: ");
        Time before = new Time(scanner.nextLine());

        //нам доступно времени (от завтрака до ужина)
        Time available = supper.minus(breakfast);

        //обед будет посередине дня
        Time lunch = breakfast.plus(available.divide(2));

        //пока между ужином и завтраком времени проходит больше, чем нужно
        while(supper.minus(breakfast).isBiggerThan(tabletInterval)) {
            //сближаем завтрак и ужин смещая и то и то на 15 минут
            breakfast = breakfast.plus(STEP);
            supper = supper.minus(STEP);
        }

        System.out.printf("Первый приём таблеток: %s%n", breakfast.minus(before));
        System.out.printf("Завтрак: %s%n", breakfast);
        System.out.printf("Обед: %s%n", lunch);
        System.out.printf("Второй приём таблеток: %s%n", supper.minus(before));
        System.out.printf("Ужин: %s%n", supper);
    }

}
